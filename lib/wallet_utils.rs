use solana_sdk::signature::{Keypair, Signer};
use serde_json;
use bs58;
// use std::fs::File;
// use std::io::Write;
// use std::path::Path;
// use hex;

pub fn calculate_numeric_key(base58_encoded_private_key: &str) -> Result<(), Box<dyn std::error::Error>> {
    // Example Base58-encoded private key (replace with actual Base58 private key)
    // let base58_encoded_private_key = "4pW3vLtTCmoiZTV4dwHZtdJKGJVFVpZFEJXJfjt4CtaXPX1uNtV1XcTWCxfKvc2vwcim6PJFugPNKGFXuwdte1d";

    // Decode the Base58 private key
    let private_key_bytes = bs58::decode(base58_encoded_private_key).into_vec()?;

    // Check if the decoded private key has the correct length (32 bytes)
    if private_key_bytes.len() != 64 {
        return Err("Decoded key must be exactly 64 bytes long.".into());
    }

    let keypair = Keypair::from_bytes(&private_key_bytes)
        .map_err(|e| format!("Failed to create keypair from bytes: {}", e))?;

    // Create a new keypair from the decoded bytes
    let mykeypair = Keypair::from_base58_string(&base58_encoded_private_key);
    let serialized_keypair = mykeypair.to_bytes();
    let serialized_vec = serialized_keypair.to_vec();
    let json_keypair = serde_json::to_string(&serialized_vec)?;

    // Output the public key
    println!("Serialized Public Key: {}", json_keypair);
    println!("JSON Public Key: {}", json_keypair);
    println!("Public Key: {}", keypair.pubkey());

    Ok(())
}

pub fn print_hello() {
    println!("Hello");
}
