use clap::{App, Arg};
use std::io::{self, Write};
use wallet_utils::{calculate_numeric_key, print_hello};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Rust CLI Library")
        .version("0.1.0")
        .author("Your Name")
        .about("Processes commands")
        .arg(Arg::new("private")
             .long("private")
             .takes_value(false)
             .help("Base58 encoded private key"))
        .get_matches();

    if matches.is_present("private") {
        println!("Enter a private key:");
        io::stdout().flush()?;

        let mut private_key = String::new();
        io::stdin().read_line(&mut private_key)?;
        let private_key = private_key.trim();

        calculate_numeric_key(private_key)?;
    } else {
        print_hello();
    }

    Ok(())
}

