# Rust Wallet Library

This Rust Wallet Library provides a set of tools for generating and managing cryptocurrency wallet keys. It is designed for developers looking to integrate wallet functionalities into their Rust applications.

## Features

- Generate cryptographic keypairs.
- Serialize keypairs into JSON format for easy storage and retrieval.
- Convert private keys to and from Base58 encoding.

## Requirements

- Rust 1.41.0 or later

## Installation

To use this library in your Rust project, add the following to your `Cargo.toml` file:

```toml
[dependencies]
client = { git = "https://github.com/joelgriffiths/rust_wallet_library.git", tag = "v0.1.0" }
```

## Usage

### Generating a Keypair

```rust
use wallet_utils::generate_keypair;

fn main() {
    let (private_key, public_key) = generate_keypair();
    println!("Private Key: {}", private_key);
    println!("Public Key: {}", public_key);
}
```

### Deserialise a Keypair
```rust
use wallet_utils::{generate_keypair, serialize_keypair};

fn main() {
    let (private_key, public_key) = generate_keypair();
    let serialized = serialize_keypair(&private_key, &public_key);
    println!("Serialized Keypair: {}", serialized);
}
```

### Deserialise a Keypair
Print the deserialized key pair

```rust
use wallet_utils::{deserialize_keypair};

fn main() {
    let json_data = "{...}";  // JSON string from serialize_keypair
    let (private_key, public_key) = deserialize_keypair(json_data).unwrap();
    println!("Private Key: {}", private_key);
    println!("Public Key: {}", public_key);
}
```

## Contributing
Contributions are welcome! Please feel free to submit a pull request or open an issue if you have ideas or feedback. Here is how you can contribute to this project:

- Fork the repository: Create your own copy of the project on GitHub.
- Clone the repository: Download your fork to your local machine.
- Create a branch: Always create a new branch for your changes.
- Make your changes: Add or modify the code as necessary for your contribution.
- Commit your changes: Make sure your commits are clear and understandable.
- Push to the branch: Upload your changes to your GitHub repository.
- Create a pull request: Open a pull request from your branch to the original repository with a clear title and description.

## License
This project is licensed under the MIT License - see the LICENSE.txt file for details.

